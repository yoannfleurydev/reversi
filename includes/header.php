<?php
    session_start();
    unset($_SESSION['game_id']);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Reversi</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="styles/style.css"/>
    <script src="scripts/jquery-2.1.3.min.js"></script>
    <script src="scripts/Board.js"></script>
    <script src="scripts/init.js"></script>
    <script src="scripts/event.js"></script>
    <script src="scripts/userExperience.js"></script>
    <script src="scripts/preferences.js"></script>
    <?php
        if (isset($_SESSION['user_id'])) {
            include_once("class/utils/PDOQueries.class.php");
            include_once("class/Game.class.php");
            $pdoQueries = new PDOQueries();
            $user_config = $pdoQueries->selectUserConfig($_SESSION['user_id']);

            if ($user_config !== NULL) {
                $stringArray = explode(";", $user_config);
                $boardColor = $stringArray[0];
                $iaColor = $stringArray[1];
                $humanColor = $stringArray[2];

                $markColor = ($stringArray[3] == 1) ? $humanColor : $boardColor;

                echo '<style>';
                echo '#board_container table {' .
                    'background: ' . $boardColor . ';' .
                    '}' .
                    '.black_piece {' .
                    'fill: ' . $humanColor . ';' .
                    '}' .
                    '.white_piece {' .
                    'fill: ' . $iaColor . ';' .
                    '}' .
                    '.marked_square {' .
                    'fill: ' . $markColor . ';' .
                    '}';
                echo '</style>';
            }
        }
    ?>
    <?php
    if (isset($_COOKIE['user_config'])) {
        $stringArray = explode(";", $_COOKIE['user_config']);
        $boardColor = $stringArray[0];
        $iaColor = $stringArray[1];
        $humanColor = $stringArray[2];

        $markColor = ($stringArray[3] == 1) ? $humanColor : $boardColor;

        echo '<style>';
        echo '#board_container table {' .
                'background: ' . $boardColor . ';' .
                '}' .
                '.black_piece {' .
                'fill: ' . $humanColor . ';' .
                '}' .
                '.white_piece {' .
                'fill: ' . $iaColor . ';' .
                '}' .
                '.marked_square {' .
                'fill: ' . $markColor . ';' .
                '}';
        echo '</style>';
    }
    ?>
</head>
    <body>
        <header>
            <h1><a href="index.php" title="Accueil">Reversi</a></h1>
            <nav id="session">
                <?php if (isset($_SESSION["user_login"])) { ?>
                    <a href="logout.php" title="Déconnexion">Déconnexion (<?php echo $_SESSION["user_login"] ?>)</a>
                <?php } else { ?>
                    <a href="login.php" title="Connexion">Connexion</a>
                    <a href="signup.php" title="Inscription">Inscription</a>
                <?php } ?>
            </nav>
            <nav id="help">
                <a href="help.php" title="Aide">Aide</a>
            </nav>
        </header>
