
    <form method="post" action="model/signupForm.php" id="signup_form">
        <fieldset>
            <legend>Inscription</legend>
            <input type="text" name="user_login" id="user_login" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{4,30}$" placeholder="Identifiant de connexion" required="required"/>
            <input type="password" name="user_pass" id="user_pass" placeholder="Mot de passe" required="required" />
            <input type="password" name="user_pass_verification" id="user_pass_verification" placeholder="Vérification du mot de passe" required="required"/>
            <input type="submit" id="submit_signup" value="Valider" />
            <p id="user_login_result"></p>
        </fieldset>
    </form>