<section>
    <h1>Bienvenue sur le Reversi</h1>
    <p>
        Ce site web est la création de deux étudiants en Licence 3 informatique
        qui devaient réaliser un site web où l'on puisse jouer au Reversi.
    </p>

    <h1>Fonctionnalité du site</h1>
    <ul>
        <li>Annuler le coup précédent <kbd>CTRL + Z</kbd></li>
        <li>Sauvegarder la partie en cours <kbd>CTRL + S</kbd></li>
        <li>Ouvrir les paramètres <kbd>CTRL + P</kbd></li>
    </ul>
</section>