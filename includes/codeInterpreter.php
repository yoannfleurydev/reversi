<?php

include_once("class/utils/ReturnCode.class.php");

if (isset($_GET["code"]) && is_numeric($_GET["code"])) {
    echo ReturnCode::printMessage($_GET["code"]);
}