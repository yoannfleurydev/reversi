<?php

include_once("class/utils/PDOQueries.class.php");
include_once("class/Game.class.php");
$pdoQueries = new PDOQueries();

?>
<?php if (isset($_SESSION['user_id'])) { ?>
<section id="user_best_games">
    <h2>Vos 10 meilleures parties</h2>
    <table>
        <tr>
            <th>Identifiant de la partie</th>
            <th>Votre score</th>
            <th>Issue de la partie</th>
        </tr>
        <?php
        $bestGames = $pdoQueries->select10BestGamesWhereUserId($_SESSION['user_id']);

        foreach($bestGames as $bestGame) {
            echo "<tr>";
            echo "<td>{$bestGame->getGameId()}</td>";
            echo "<td>{$bestGame->getGameScore()}</td>";
            if ($bestGame->getGameVictory() == NULL) {
                echo "<td>Partie en cours</td>";
            } elseif ($bestGame->getGameVictory() == 1) {
                echo "<td>Victoire</td>";
            } else {
                echo "<td>Défaite</td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
</section>
<?php } ?>
<section id="best_games">
    <h2>Les 10 meilleures parties globales</h2>
    <table>
        <tr>
            <th>Identifiant de la partie</th>
            <th>Identifiant du joueur</th>
            <th>Score du joueur partie</th>
            <th>Issue</th>
        </tr>
        <?php
        $bestGames = $pdoQueries->select10BestGames();

        foreach($bestGames as $bestGame) {
            $user = $pdoQueries->selectUserWhereUserId($bestGame->getGameUserId());
            echo "<tr>";
            echo "<td>{$bestGame->getGameId()}</td>";
            echo "<td>{$user->getUserLogin()}</td>";
            echo "<td>{$bestGame->getGameScore()}</td>";
            if ($bestGame->getGameVictory() == NULL) {
                echo "<td>Partie en cours</td>";
            } elseif ($bestGame->getGameVictory() == 1) {
                echo "<td>Victoire</td>";
            } else {
                echo "<td>Défaite</td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
</section>
<?php if (isset($_SESSION['user_id'])) { ?>
    <section id="user_stats">
        <h2>Vos statistiques</h2>
        <?php

        $games = $pdoQueries->selectGamesWhereGameUserId($_SESSION['user_id']);

        $nbGamePlayed = count($games);
        $nbGameWin = 0;
        $gamesScore = 0;
        $cptGameScore = 0;
        foreach ($games as $game) {
            if ($game->getGameVictory() == 1) {
                $nbGameWin++;
            }

            if ($game->getGameScore() != NULL) {
                $gamesScore += $game->getGameScore();
                $cptGameScore++;
            }
        }
        $percentageGameWin = ($nbGameWin / $nbGamePlayed) * 100;


        echo "<p>Vous avez jouez <strong>$nbGamePlayed</strong> parties et gagné
                <strong>$percentageGameWin%</strong> d'entre elles, c'est à dire
                <strong>$nbGameWin</strong>.";

        if ($cptGameScore > 0) {
            $meanScore = $gamesScore / $cptGameScore;
            echo " Vous avez un score moyen de <strong>$meanScore</strong>";
        }
        echo "</p>";
        ?>
    </section>
<?php } ?>
<section id="global_stats">
    <h2>Statistiques globales</h2>
    <?php

    $games = $pdoQueries->selectGames();

    $nbGamePlayed = count($games);
    $nbGameWin = 0;
    $gamesScore = 0;
    $cptGameScore = 0;
    foreach ($games as $game) {
        if ($game->getGameVictory() == 1) {
            $nbGameWin++;
        }

        if ($game->getGameScore() != NULL) {
            $gamesScore += $game->getGameScore();
            $cptGameScore++;
        }
    }
    $percentageGameWin = ($nbGameWin / $nbGamePlayed) * 100;


    echo "<p>En tout, ce sont <strong>$nbGamePlayed</strong> parties jouées, dont
                <strong>$percentageGameWin%</strong> d'entre elles de gagnées, c'est à dire
                <strong>$nbGameWin</strong>.";

    if ($cptGameScore > 0) {
        $meanScore = $gamesScore / $cptGameScore;
        echo " Le score moyen est de <strong>$meanScore</strong>";
    }
    echo "</p>";
    ?>
</section>