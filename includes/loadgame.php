<section id="loadgame">
    <h2>Charger une partie</h2>
    <?php
    include_once('class/utils/PDOQueries.class.php');
    include_once('class/Game.class.php');

    $pdo_queries = new PDOQueries();
    $games = $pdo_queries->selectGamesWhereGameUserId($_SESSION['user_id']);

    if (count($games) == 0) {
        echo "<p class='error'>Aucune données n'est disponible.</p>";
    } else {
        echo "<table>";
        echo "<tr><th>Issue</th><th>Score</th><th>Charger</th></tr>";

        foreach($games as $game) {
            echo "<tr>";
            if ($game->getGameVictory() != NULL) {
                if ($game->getGameVictory() == 1) {
                    echo "<td>Victoire</td>";
                } else {
                    echo "<td>Défaite</td>";
                }
            } else {
                echo "<td>Partie non terminée</td>";
            }
            if ($game->getGameScore() != NULL) {
                echo "<td>{$game->getGameScore()}</td>";
            } else {
                echo "<td>Partie non terminée</td>";
            }

            if ($game->getGameVictory() == NULL) {
                echo "<td><button class='loadGame' value='{$game->getGameId()}'>Charger</button></td>";
            } else {
                echo "<td></td>";
            }
        }
        echo "</table>";
    }
    ?>
</section>