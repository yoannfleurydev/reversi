<?php

session_start();
unset($_SESSION);
session_destroy();
unset($_COOKIE);
setcookie("game_id", "", time() - 3600, '/');
header("Location: index.php");