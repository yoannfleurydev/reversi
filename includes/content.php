<section id="gameboard">
    <h2>Plateau de jeu</h2>
    <div id="board_container"></div>
    <script>
        var plateau = new Board();
        plateau.markPlayableSquares();
        plateau.drawBoard();
        setInterval("plateau.iaClick()", 350);
    </script>
    <?php
    if (isset($_COOKIE['game_id']) && is_string($_COOKIE['game_id']) && strlen($_COOKIE['game_id']) > 0) {
        include_once("class/utils/PDOQueries.class.php");
        include_once("class/Game.class.php");
        $pdoQueries = new PDOQueries();

        $loadGame = $pdoQueries->selectGame($_COOKIE['game_id']);

        $saveString = $loadGame->getGameSave();
        echo "<script>plateau.loadState('$saveString');</script>";
    }
    ?>
</section>
<section id="options">
    <h2>Options</h2>
    <button id="preferences">Préférences</button>
    <?php if (isset($_SESSION["user_login"])) { ?>
        <a href="loadgame.php" title="Charger">Charger</a>
    <?php } else { ?>
        <button id="loadState">Charger</button>
    <?php } ?>
    <button id="saveState">Sauvegarder</button>
    <a href="statistics.php">Statistiques</a>
    <button id="new_game">Nouvelle Partie</button>
</section>