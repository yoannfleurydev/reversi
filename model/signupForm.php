<?php
include_once("../class/utils/ReturnCode.class.php");
include_once ("../class/utils/PDOQueries.class.php");

$pdoQueries = new PDOQueries();

// Il y a surement moyen de créer une fonction de vérification "générale".
if (isset($_POST) &&
    isset($_POST["user_login"]) && is_string($_POST["user_login"]) && strlen($_POST["user_login"]) >= 5 &&
    isset($_POST["user_pass"]) && is_string($_POST["user_pass"]) && strlen($_POST["user_pass"]) >= 6 &&
    isset($_POST["user_pass_verification"]) && is_string($_POST["user_pass_verification"]) &&
    $_POST["user_pass"] === $_POST["user_pass_verification"]) {

    // Vérification du login
    $user_login = null;
    if (htmlspecialchars($_POST['user_login']) == $_POST['user_login']) {
        $user_login = $_POST['user_login'];
    } else {
        exitScript(ReturnCode::$INVALID_USER_LOGIN);
    }

    // Vérification du mot de passe.
    $user_pass = null;
    if (htmlspecialchars($_POST['user_pass']) == $_POST['user_pass']) {
        $options = array('cost' => 11);
        $user_pass = password_hash($_POST['user_pass'], PASSWORD_BCRYPT, $options);
    } else {
        exitScript(ReturnCode::$INVALID_USER_PASS);
    }

    $result = $pdoQueries->selectUser($user_login);

    // Si c'est null, c'est qu'il n'y pas d'utilisateur avec ce pseudo.
    if ($result == null) {
        $pdoQueries->insertUser($user_login, $user_pass);
        exitScript(ReturnCode::$VALID_SIGNUP);
    } else {
        if (strtolower($result->getUserLogin()) == strtolower($user_login)) {
            exitScript(ReturnCode::$USER_LOGIN_EXISTS);
        } else {
            $pdoQueries->insertUser($user_login, $user_pass);
            exitScript(ReturnCode::$VALID_SIGNUP);
        }
    }
} else {
    exitScript(ReturnCode::$INVALID_USER_LOGIN + ReturnCode::$INVALID_USER_PASS);
}

function exitScript($errorCode = NULL) {
    $LOCATION = "Location: ../index.php";
    if ($errorCode != NULL) {
        $LOCATION .= "?code=" . $errorCode;
    }

    header($LOCATION);
    exit;
}