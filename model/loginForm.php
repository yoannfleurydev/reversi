<?php

session_start();

include_once("../class/utils/PDOQueries.class.php");
include_once("../class/utils/ReturnCode.class.php");

if (isset($_POST) &&
    isset($_POST["user_login"]) && is_string($_POST["user_login"]) &&
    isset($_POST["user_pass"]) && is_string($_POST["user_pass"])) {

    // Les variables sont bien existantes alors on les affecte à des variables locales.
    $user_login = $_POST['user_login'];
    $user_pass  = $_POST['user_pass'];

    // On crée notre objet d'accès à la base de données.
    $pdoQueries = new PDOQueries();
    // On sélectionne l'utilisateur correspondant aux crédits.
    $users = $pdoQueries->selectUsers();

    foreach ($users as $user) {
        if ($user->getUserLogin() === $user_login) {
            if (password_verify($user_pass, $user->getUserPass())) {
                $_SESSION['user_id'] = $user->getUserId();
                $_SESSION['user_login'] = $user->getUserLogin();
                $_SESSION['user_config'] = $user->getUserConfig();

                if (isset($_COOKIE['user_config'])) {
                    unset($_COOKIE['user_config']);
                    setcookie('user_config', '', time() - 3600, '/');
                }

                header("Location: ../index.php");
                exit;
            } else {
                header("Location: ../login.php?code=" . ReturnCode::$WRONG_USER_PASS);
                exit;
            }
        }
    }
    header("Location: ../login.php?code=" . ReturnCode::$INVALID_USER_LOGIN);
    exit;
}