$(function() {
    $("#preferences").click(function () {
        var section  = $("#preferencesDialog");
        if (section.length) {
            section.remove();
        } else {
            $("#options").append('<section id="preferencesDialog">' +
            '<h3>Préférences</h3>' +
            '<p>Couleur humain</p>' +
            '<input type="color" id="humanColor" value="#000000" />' +
            '<p>Couleur IA</p>' +
            '<input type="color" id="iaColor" value="#FFFFFF" />' +
            '<p>Couleur plateau</p>' +
            '<input type="color" id="boardColor" value="#008000"/>' +
            '<p>Aide</p>' +
            '<input type="checkbox" id="checkboxHelp"/>' +
            '<button id="applyPreferences">Appliquer</button>' +
            '</section>');
        }
    });
});

$(document).on("click", "#applyPreferences", function() {
    var humanColor = $("#humanColor").val();
    var iaColor = $("#iaColor").val();
    var boardColor = $("#boardColor").val();
    var checkHelp = 0;
    if ($("#checkboxHelp").prop("checked")) {
        checkHelp = 1;
    }

    $.ajax({
        url: 'scripts/ajax/setPreferences.php',
        type: 'POST',
        data: {
            humanColor: humanColor,
            iaColor: iaColor,
            boardColor: boardColor,
            helpCheck: checkHelp
        },
        success: function () {
            var markColor;
            if (checkHelp === 1) {
                markColor = humanColor;
            } else {
                markColor = boardColor;
            }

            $("style").remove();
            $("head").append('<style type="text/css">' +
            '#board_container table {' +
            'background: ' + boardColor + ';' +
            '}' +
            '.black_piece {' +
            'fill: ' + humanColor + ';' +
            '}' +
            '.white_piece {' +
            'fill: ' + iaColor + ';' +
            '}' +
            '.marked_square {' +
            'fill: ' + markColor + ';' +
            '}' +
            '</style>');
        },
        error: function (e) {
            console.log(e.message);
        }
    });
});



$(function () {
    $("#loadPreferences").click(function () {
        $.ajax({url: 'scripts/ajax/loadPreferences.php',
               type: 'GET',
               data: {},
               success: function (data) {
                   if (data === "") {
                        // Le script n'a pas fonctionné
                        return;
                    }
                    
                    var configs = data.split(";");
                    
                    var colorMark;
                    if (configs[3] === 1) {
                        colorMark = colorHuman;
                    } else {
                        colorMark = colorBoard;
                    }
                    
                    var head = document.getElementsByName("head");
                    
                    var css;
                    if ((css = document.getElementsByTagName("style")) !== null) {
                        head.removeChild(css);
                    }
                    
                    css = '<style type="text/css">'             +
                          '#board_container table {'            +
                              'background: ' + configs[0] + ';' +
                          '}'                                   +
                          '.black_piece {'                      +
                              'fill: ' + configs[2] + ';'       +
                          '}'                                   +
                          '.white_piece {'                      +
                              'fill: ' + configs[1] + ';'       +
                          '}'                                   +
                          '.marked_square {'                    +
                              'fill: ' + colorMark + ';'        +
                          '}'                                   +
                          '</style>';
                      
                    head.appendChild(css);
               },
               error: function (e) {
                   console.log(e.message);
               }
        });
    });
});
