// Cancel last move
$(document).keydown(function(e){
    if (e.keyCode==90 && e.ctrlKey) {
        plateau.cancelLastMove();
    }
});

// Save game status
$(document).keydown(function(e){
    if (e.keyCode==83 && e.ctrlKey) {
        e.preventDefault();      // Permet de prendre le dessus sur le navigateur.
        saveState();
    }
});
$(function() {
    $("#saveState").click(function() {
        saveState();
    });
});


$(function () {
    $("#loadState").click(function () {
        $("#status").remove();
        $(".valid").remove();
        if ($("#state").val() === undefined) {
            $("#options").append('<textarea placeholder="Copier ici la chaîne de sauvegarde" id="state"></textarea>');
        } else {
            var state = $('#state').val();
            if (state.length == 64) {
                plateau.loadState(state);
                $("#status").remove();
                $("#state").remove();
                $("#options").append("<p class='valid' id='status'>Votre partie a été chargée</p>");
            } else {
                $("#status").remove();
                $("#state").remove();
                $("#options").append("<p class='error' id='status'>Une erreur s'est produite.</p>");
            }
        }
    });
});

$(function () {
    $(".loadGame").click(function () {
        document.cookie="game_id=" + $(this).val() + "; path=/";
        window.location.replace("index.php");
    });
});

function saveState() {
    $.ajax({
        url: 'scripts/ajax/save_game.php',
        type: 'POST',
        data: { game_save: plateau.toString() },
        success: function (data) {
            if (data == "true") {
                $(".error").remove();
                $("#state").remove();
                $(".valid").remove();
                $("#options").append("<p class='valid' id='status'>Votre progression a été sauvegardée</p>");
            } else {
                $(".error").remove();
                $("#state").remove();

                var validSelector = $(".valid");
                if (validSelector.length) {
                    $(".valid").remove();
                } else {
                    $("#options").append("<p class='valid'>Voici la sauvegarde de votre partie : <br />"
                        + plateau.toString() + "</p>");
                }
            }
        },
        error: function (e) {
            // Appelé quand il y a une erreur.
            console.log(e.message);
        }
    });
}

$(document).on("click", "#replay_modal", function() {
    document.cookie = "game_id=; expires=Thu, 01 Jan 1970 00;00;00 UTC; path=/";
    location.reload();
});

$(document).on("click", "#quit_modal", function() {
    $("#overlay").remove();
    $("#endFrame").remove();
});

// Fonction qui retire les notifications.
$(document).on("click", "#status", function() {
    $(this).remove();
});

$(function() {
    $("#new_game").click(function(){
        document.cookie = "game_id=; expires=Thu, 01 Jan 1970 00;00;00 UTC; path=/";
        location.reload();
    });
});
