// Dans ce fichier se trouve les optimisation ergonomique du site
// Par exemple, quand un utilisateur rentre un identifiant de connexion,
//   une requête est envoyée pour vérifier si cet identifiant n'existe
//   pas déjà.


// Fonction d'analyse de l'identifiant de connexion.
$(function() {
    $("#signup_form #user_login").keypress(function(e) {
        var user_login = $("input#user_login").val();
        if (e.keyCode == 8) {
            user_login  = user_login.substring(0, user_login.length -1);
        } else {
            user_login += String.fromCharCode(e.which);
        }

        if (user_login.length > 5) {
            $.ajax({
                url: 'scripts/ajax/user_login_valid.php',
                type: 'GET',
                data: 'user_login=' + user_login,
                success: function (data) {
                    // Appelé quand il y a succes.
                    if (data == "true") {
                        $('#user_login_result').html("Cet identifiant est disponible")
                            .css("background", "#27AE60")
                            .css("border", "3px solid #2ECC71");
                        $('input#user_login').css("color", "#2ECC71")
                            .css("border-color", "#2ECC71");
                        $('#submit_signup').prop('disabled', false);
                    } else {
                        $('#user_login_result').html("Cet identifiant n'est pas disponible")
                            .css("background", "#C0392B")
                            .css("border", "3px solid #E74C3C");
                        $('input#user_login').css("color", "#E74C3C")
                            .css("border-color", "#E74C3C")
                            .css("box-shadow", "none");
                        $('#submit_signup').prop('disabled', true);
                    }
                },
                error: function (e) {
                    // Appelé quand il y a une erreur.
                    console.log(e.message);
                }
            });
        } else {
            $('#user_login_result').html("")
                .css("background", "none")
                .css("border", "none");
            $('input#user_login').css("color", "#000")
                .css("border-color", "#BDC3C7");
        }
    });
});


// Fonction d'analyse de mot de passe.
$(function() {
    $('#user_pass_verification').keyup(function(e) {
        var user_pass = $('#user_pass');
        var user_pass_verification = $('#user_pass_verification');
        if (user_pass.val().length === user_pass_verification.val().length) {
            console.log(user_pass.val());
            if (user_pass.val() != user_pass_verification.val()) {
                $('#user_login_result').html("Vérification du mot de passe incorrecte")
                    .css("background", "#C0392B")
                    .css("border", "3px solid #E74C3C");
                $('#user_pass_verification').css("color", "#E74C3C")
                    .css("border-color", "#E74C3C")
                    .css("box-shadow", "none");
                $('#submit_signup').prop('disabled', true);
            } else {
                $('#user_login_result').html("Les mots de passe sont identiques")
                    .css("background", "#27AE60")
                    .css("border", "3px solid #2ECC71");
                $('#user_pass_verification').css("color", "#2ECC71")
                    .css("border-color", "#2ECC71");
                $('#submit_signup').prop('disabled', false);
            }
        } else {
            $('#user_login_result').html("")
                .css("background", "none")
                .css("border", "none");
        }
    });
});
