<?php

if (isset($_SESSION['user_id'])) {
    // L'utilisateur est connecté
    $pdo_queries = new PDOQueries();
    return $pdo_queries->selectUserConfig($_SESSION['user_id']);
} else {
    // L'utilisateur n'est pas connecté
    if (isset($_COOKIE['user_config'])) {
        // L'utilisateur a un cookie de configuration
        return $_COOKIE['user_config'];
    }
}

