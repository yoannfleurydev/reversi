<?php

session_start();

include_once("../../class/utils/PDOQueries.class.php");

if (isset($_POST['boardColor']) && isset($_POST['iaColor']) && isset($_POST['humanColor'])
    && isset($_POST['helpCheck'])) {
    $user_config = $_POST['boardColor'] . ';' .
        $_POST['iaColor'] . ';' .
        $_POST['humanColor'] . ';' .
        $_POST['helpCheck'];

    if (isset($_SESSION['user_id'])) {
        // L'utilisateur est connecté
        $pdo_queries = new PDOQueries();
        $pdo_queries->updateUserConfig($_SESSION['user_id'], $user_config);
        unset($_COOKIE['user_config']);
        setcookie("user_config", '', time() - 3600, "/");
    } else {
        unset($_COOKIE['user_config']);
        // L'utilisateur n'est pas connecté
        setcookie("user_config", $user_config, time() + 60 * 60 * 24 * 30, "/");
    }
    echo "true";
} else {
    echo "false";
}