<?php

include_once('../../class/utils/PDOQueries.class.php');

$pdo_queries = new PDOQueries();

$user_login = htmlspecialchars(strip_tags($_GET['user_login']));

$results = $pdo_queries->selectUser($user_login);

if ($results != null) {
    echo "false";
} else {
    echo "true";
}