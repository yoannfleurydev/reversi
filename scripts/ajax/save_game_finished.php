<?php

include_once('../../class/utils/PDOQueries.class.php');
include_once('../../class/Game.class.php');
$pdo_queries = new PDOQueries();

session_start();

if (isset($_SESSION['user_id']) && isset($_POST['game_save']) &&
    isset($_POST['game_score']) && isset($_POST['game_victory'])) {
    if (isset($_COOKIE['game_id'])) {
        $pdo_queries->updateGameFinished($_COOKIE['game_id'], $_POST['game_save'], $_POST['game_score'], $_POST['game_victory']);
    } else {
        $pdo_queries->insertGameFinished($_SESSION['user_id'], $_POST['game_save'], $_POST['game_score'], $_POST['game_victory']);
        $_SESSION['game'] = $pdo_queries->selectLastGame($_SESSION['user_id']);
        $_COOKIE['game_id'] = $_SESSION['game']->getGameId();
    }
    echo "true";
} else {
    echo "false";
}