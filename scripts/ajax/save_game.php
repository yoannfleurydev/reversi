<?php

include_once('../../class/utils/PDOQueries.class.php');
include_once('../../class/Game.class.php');
$pdo_queries = new PDOQueries();

session_start();

if (isset($_SESSION['user_id']) && isset($_POST['game_save'])) {
    if (isset($_COOKIE['game_id'])) {
        $pdo_queries->updateGame($_COOKIE['game_id'], $_POST['game_save']);
    } else {
        $pdo_queries->insertGame($_SESSION['user_id'], $_POST['game_save']);
        $_SESSION['game'] = $pdo_queries->selectLastGame($_SESSION['user_id']);
        $_COOKIE['game_id'] = $_SESSION['game']->getGameId();
    }
    echo "true";
} else {
    echo "false";
}