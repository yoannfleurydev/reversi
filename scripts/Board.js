function Board() {

    // ATTRIBUTS
    
    this.board = [];
    this.player   = 'X';
    this.opponent = 'O';
    this.ia = 'O'; 
    this.playerIsBlocked = false;
    this.opponentIsBlocked = false;
    this.save = [];
    this.lastPlayer = 'X';
    
    // CONSTRUCTEUR

    for (var line = 0; line < 8; line++) {
        this.board[line] = [];
        for (var col = 0; col < 8; col++) {
            this.board[line][col] = '-';
        }
    }
    
    this.board[3][3] = 'O';
    this.board[3][4] = 'X';
    this.board[4][4] = 'O';
    this.board[4][3] = 'X';
    
    // FONCTIONS
    
    /**
     * Curseur entre dans la case courante
     * Afficher la prévisualisation de l'action
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.mouseIn = function (x, y) {
        console.log("mouseIn(" + x + "," + y + ")");
    };

    /**
     * Curseur sort de la case courante
     * Réinitialiser la prévisulisation de l'action
     * @returns {undefined}
     */
    this.mouseOut = function () {
        console.log("mouseOut()");
    };
    
    /**
     * Effectue l'action prévisualisée
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.mouseClick = function (x, y) {
        this.saveState();
        this.putPiece(x, y);
    };
    
    /**
     * Algorithme d'intelligence artificielle
     * @returns {undefined}
     */
    this.iaClick = function () {
        if (this.player === this.ia && !this.isTheEndOfGame()) {
            var tabX = [];
            var tabY = [];
            var tabXtoPrioritize = [];
            var tabYtoPrioritize = [];
            
            var i = 0;
            var j = 0;
            for (var line = 0; line < 8; line++) {
                for (var col = 0; col < 8; col++) {
                    if (this.board[line][col] === "m") {
                        // Si on peut jouer dans un coin alors on le fait
                        if (this.isACorner(col, line)) {
                            this.putPiece(col, line);
                            return;
                        }
                        
                        if (!this.isAdjacentToACorner(col, line)) {
                            tabXtoPrioritize[j] = col;
                            tabYtoPrioritize[j] = line;
                            j++;
                        }
                        
                        tabX[i] = col;
                        tabY[i] = line;
                        i++;
                    }
                }
            }
            
            // On essaie d'éviter les cases adjacentes aux coins
            if (tabXtoPrioritize.length !== 0) {
                var randNumber = Math.round(Math.random() * 
                                            (tabXtoPrioritize.length - 1));
                this.putPiece(tabXtoPrioritize[randNumber],
                              tabYtoPrioritize[randNumber]);
                return;
            }
            
            var randNumber = Math.round(Math.random() * (i - 1));
            this.putPiece(tabX[randNumber], tabY[randNumber]);
        }
    };
    
    /**
     * Détermine si la case de coordonnées (x, y) est un coin du plateau
     * @param {Number} x
     * @param {Number} y
     * @returns {Boolean}
     */
    this.isACorner = function (x, y) {
        return x === 0 && y === 0 ||
               x === 7 && y === 7 ||
               x === 0 && y === 7 ||
               x === 7 && y === 0;
    };
    
    /**
     * Détermine si la case de coordonnées (x, y) est adjacente à un coin du 
     * plateau
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.isAdjacentToACorner = function (x, y) {
        return x === 0 && y === 1 ||
               x === 1 && y === 0 ||
               x === 1 && y === 1 ||
               
               x === 6 && y === 7 ||
               x === 7 && y === 6 ||
               x === 6 && y === 6 ||
               
               x === 0 && y === 6 ||
               x === 1 && y === 7 ||
               x === 1 && y === 6 ||
               
               x === 6 && y === 0 ||
               x === 7 && y === 1 ||
               x === 6 && y === 1;
    };
    
    /**
     * Joue le pion en (x, y)
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.putPiece = function (x, y) {
        this.board[y][x] = this.player;
        this.replacePieces(x, y);
        this.removeMarkedSquares();
        
        var i = 0;
        do {
            this.switchPlayersJob();
            this.markPlayableSquares();
            this.showPlayerGameplay();
            this.drawBoard();
            i++;
        } while (this.playerIsBlocked && i < 2);
        
        if (this.isTheEndOfGame()) {
            this.giveTheWinner();
        }
    };
    
    /**
     * Permute les rôles des joueurs 
     * @returns {undefined}
     */
    this.switchPlayersJob = function () {
        if (this.player === 'X') {
            this.player = 'O';
            this.opponent = 'X';
        } else {
            this.player = 'X';
            this.opponent = 'O';
        }
        
        var tmp = this.playerIsBlocked;
        this.playerIsBlocked = this.opponentIsBlocked;
        this.opponentIsBlocked = tmp;
    };
    
    /**
     * Remplace les pions suite à l'action du joueur courant
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePieces = function (x, y) {
        if (x - 1 >= 0 && y - 1 >= 0) {
            if (this.board[y - 1][x - 1] === this.opponent) {
                this.replacePiecesAtNW(x - 1, y - 1);
            }
        }
        
        if (y - 1 >= 0) {
            if (this.board[y - 1][x] === this.opponent) {
                this.replacePiecesAtN(x, y - 1);
            }
        }
        
        if (x + 1 < 8 && y - 1 >= 0) {
            if (this.board[y - 1][x + 1] === this.opponent) {
                this.replacePiecesAtNE(x + 1, y - 1);
            }
        }
        
        if (x + 1 < 8) {
            if (this.board[y][x + 1] === this.opponent) {
                this.replacePiecesAtE(x + 1, y);
            }
        }
        
        if (x + 1 < 8 && y + 1 < 8) {
            if (this.board[y + 1][x + 1] === this.opponent) {
                this.replacePiecesAtSE(x + 1, y + 1);
            }
        }
        
        if (y + 1 < 8) {
            if (this.board[y + 1][x] === this.opponent) {
                this.replacePiecesAtS(x, y + 1);
            }
        }
        
        if (x - 1 >= 0 && y + 1 < 8) {
            if (this.board[y + 1][x - 1] === this.opponent) {
                this.replacePiecesAtSW(x - 1, y + 1);
            }
        }
        
        if (x - 1 >= 0) {
            if (this.board[y][x - 1] === this.opponent) {
                this.replacePiecesAtW(x - 1, y);
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Nord-Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtNW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && curY >= 0 && this.board[curY][curX] === this.opponent) {
            curX--;
            curY--;
        }
        
        if (curX >= 0 && curY >= 0 && this.board[curY][curX] === this.player) {
            while (curX <= x && curY <= y) {
                this.board[curY][curX] = this.player;
                curX++;
                curY++;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Nord
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtN = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curY >= 0 && this.board[curY][curX] === this.opponent) {
            curY--;
        }
        
        if (curY >= 0 && this.board[curY][curX] === this.player) {
           while (curY <= y) {
                this.board[curY][curX] = this.player;
                curY++;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Nord-Est
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtNE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && curY >= 0 && this.board[curY][curX] === this.opponent) {
            curX++;
            curY--;
        }
        
        if (curX < 8 && curY >= 0 && this.board[curY][curX] === this.player) {
           while (curX >= x && curY <= y) {
                this.board[curY][curX] = this.player;
                curX--;
                curY++;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Est
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && this.board[curY][curX] === this.opponent) {
            curX++;
        }
        
        if (curX < 8 && this.board[curY][curX] === this.player) {
           while (curX >= x) {
                this.board[curY][curX] = this.player;
                curX--;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Sud-Est
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtSE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && curY < 8 && this.board[curY][curX] === this.opponent) {
            curX++;
            curY++;
        }
        
        if (curX < 8 && curY < 8 && this.board[curY][curX] === this.player) {
           while (curX >= x && curY >= y) {
                this.board[curY][curX] = this.player;
                curX--;
                curY--;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Sud
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtS = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curY < 8 && this.board[curY][curX] === this.opponent) {
            curY++;
        }
        
        if (curY < 8 && this.board[curY][curX] === this.player) {
           while (curY >= y) {
                this.board[curY][curX] = this.player;
                curY--;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Sud-Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtSW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && curY < 8 && this.board[curY][curX] === this.opponent) {
            curX--;
            curY++;
        }
        
        if (curX >= 0 && curY < 8 && this.board[curY][curX] === this.player) {
           while (curX <= x && curY >= y) {
                this.board[curY][curX] = this.player;
                curX++;
                curY--;
            }
        }
    };
    
    /**
     * Incrémente l'itération dans la direction Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.replacePiecesAtW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && this.board[curY][curX] === this.opponent) {
            curX--;
        }
        
        if (curX >= 0 && this.board[curY][curX] === this.player) {
           while (curX <= x) {
                this.board[curY][curX] = this.player;
                curX++;
            }
        }
    };

    /**
     * Identifie et marque les cases jouables
     * @returns {undefined}
     */
    this.markPlayableSquares = function () {
        this.playerIsBlocked = true;
        for (line = 0; line < 8; line++) {
            for (col = 0; col < 8; col++) {
                if (this.isPlayable(col, line) && this.board[line][col] === '-') {
                    this.board[line][col] = 'm';
                    this.playerIsBlocked = false;
                }
            }
        }
    };
    
    /**
     * Réinitialise les case marquées comme jouables
     * @returns {undefined}
     */
    this.removeMarkedSquares = function () {
        for (var line = 0; line < 8; line++) {
            for (var col = 0; col < 8; col++) {
                if (this.board[line][col] === 'm') {
                    this.board[line][col] = '-';
                }
            }
        }
    };
    
    /**
     * Détermine si la case courante à un pion de l'adversaire adjacent
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.isPlayable = function (x, y) {
        if (x - 1 >= 0 && y - 1 >= 0) {
            if (this.board[y - 1][x - 1] === this.opponent) {
                if (this.haveANeighborAtNW(x - 1, y - 1)) {
                    return true;
                }
            }
        }
        
        if (y - 1 >= 0) {
            if (this.board[y - 1][x] === this.opponent) {
                if (this.haveANeighborAtN(x, y - 1)) {
                    return true;
                }
            }
        }
        
        if (x + 1 < 8 && y - 1 >= 0) {
            if (this.board[y - 1][x + 1] === this.opponent) {
                if (this.haveANeighborAtNE(x + 1, y - 1)) {
                    return true;
                }
            }
        }
        
        if (x + 1 < 8) {
            if (this.board[y][x + 1] === this.opponent) {
                if (this.haveANeighborAtE(x + 1, y)) {
                    return true;
                }
            }
        }
        
        if (x + 1 < 8 && y + 1 < 8) {
            if (this.board[y + 1][x + 1] === this.opponent) {
                if (this.haveANeighborAtSE(x + 1, y + 1)) {
                    return true;
                }
            }
        }
        
        if (y + 1 < 8) {
            if (this.board[y + 1][x] === this.opponent) {
                if (this.haveANeighborAtS(x, y + 1)) {
                    return true;
                }
            }
        }
        
        if (x - 1 >= 0 && y + 1 < 8) {
            if (this.board[y + 1][x - 1] === this.opponent) {
                if (this.haveANeighborAtSW(x - 1, y + 1)) {
                    return true;
                }
            }
        }
        
        if (x - 1 >= 0) {
            if (this.board[y][x - 1] === this.opponent) {
                 if (this.haveANeighborAtW(x - 1, y)) {
                     return true;
                 }
            }
        }
        
        return false;
    };
    
    /**
     * Incrémente l'itération dans la direction Nord-Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtNW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && curY >= 0 && this.board[curY][curX] === this.opponent) {
            curX--;
            curY--;
        }

        return curX >= 0 && curY >= 0 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Nord
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtN = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curY >= 0 && this.board[curY][curX] === this.opponent) {
            curY--;
        }
        
        return curY >= 0 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Nord-Est
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtNE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && curY >= 0 && this.board[curY][curX] === this.opponent) {
            curX++;
            curY--;
        }

        return curX < 8 && curY >= 0 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Est
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && this.board[curY][curX] === this.opponent) {
            curX++;
        }

        return curX < 8 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Sud-Est
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtSE = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX < 8 && curY < 8 && this.board[curY][curX] === this.opponent) {
            curX++;
            curY++;
        }

        return curX < 8 && curY < 8 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Sud
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtS = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curY < 8 && this.board[curY][curX] === this.opponent) {
            curY++;
        }

        return curY < 8 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Sud-Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtSW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && curY < 8 && this.board[curY][curX] === this.opponent) {
            curX--;
            curY++;
        }

        return curX >= 0 && curY < 8 && this.board[curY][curX] === this.player;
    };
    
    /**
     * Incrémente l'itération dans la direction Ouest
     * @param {Number} x
     * @param {Number} y
     * @returns {boolean}
     */
    this.haveANeighborAtW = function (x, y) {
        var curX = x;
        var curY = y;
        
        while (curX >= 0 && this.board[curY][curX] === this.opponent) {
            curX--;
        }

        return curX >= 0 && this.board[curY][curX] === this.player;
    };
 
    /**
     * Informe les utilisateurs sur l'état actuel du tour
     * @returns {undefined}
     */
    this.showPlayerGameplay = function () {
        if (this.playerIsBlocked) {
            console.log(this.player + " ne peut pas jouer."); // En attendant l'implémentation du CSS
        } else {
            console.log(this.player + " peut jouer."); // En attendant l'implémentation du CSS
        }
    };
 
    /**
     * Détermine si la partie est terminée
     * @returns {Boolean}
     */
    this.isTheEndOfGame = function () {
        return this.playerIsBlocked && this.opponentIsBlocked;
    };
   
    /**
     * Détermine le joueur qui a gagné la partie
     * @returns {undefined}
     */
    this.giveTheWinner = function () {
        var blackPiecesCpt = 0;
        var whitePiecesCpt = 0;

        for (line = 0; line < 8; line++) {
            for (col = 0; col < 8; col++) {
                if (this.board[line][col] === 'X') {
                    blackPiecesCpt++;
                }

                if (this.board[line][col] === 'O') {
                    whitePiecesCpt++;
                }
            }
        }

        var body = document.getElementsByTagName("body")[0];
        var overlay = document.createElement("div");
        overlay.setAttribute("id", "overlay");
        body.appendChild(overlay);
        var section = document.createElement("section");
        section.setAttribute("id", "endFrame");
        var title = document.createElement("h1");

        var game_save = this.toString();
        var game_score = blackPiecesCpt;
        var game_victory;
        if (blackPiecesCpt < whitePiecesCpt) {
            title.innerHTML = "DEFAITE";
            game_victory = 0;
        } else if (whitePiecesCpt < blackPiecesCpt) {
            title.innerHTML = "VICTOIRE";
            game_victory = 1;
        } else {
            title.innerHTML = "EGALITE";
            game_victory = 0;
        }

        section.appendChild(title);
        var replay = document.createElement("button");
        replay.setAttribute("id", "replay_modal");
        replay.innerHTML = "Rejouer";
        section.appendChild(replay);
        var quit = document.createElement("button");
        quit.setAttribute("id", "quit_modal");
        quit.innerHTML = "Quitter";
        section.appendChild(quit);
        body.appendChild(section);

        this.saveGameFinished(game_save, game_score, game_victory);
    };
    
    /**
     * Dessine le plateau actuel sur la page
     * @returns {undefined}
     */
    this.drawBoard = function () {
        var container = document.getElementById("board_container");
        container.innerHTML = "";
        var table = document.createElement("table");
        
        for (var line = 0; line < 8; line++) {
            var tr = document.createElement("tr");
            for (var col = 0; col < 8; col++) {
                if (this.board[line][col] === 'O') {
                    this.drawWhitePiece(tr);
                }
                
                if (this.board[line][col] === 'X') {
                    this.drawBlackPiece(tr);
                }
                
                if (this.board[line][col] === 'm') {
                    this.drawMarkedSquare(tr, col, line);
                }
                
                if (this.board[line][col] === '-') {
                    this.drawVoidSquare(tr);
                }
            }
            table.appendChild(tr);
        }
        container.appendChild(table);
    };
    
    /**
     * Dessine un pion noir sur le plateau
     * @param {Element} tr
     * @returns {undefined}
     */
    this.drawBlackPiece = function (tr) {
        var black_piece = '<svg width="40" height="40"><circle cx="20" cy="20" r="20" stroke="black" stroke-width="0" class="black_piece" /></svg>';
        var td = document.createElement("td");
        td.innerHTML = black_piece;
        tr.appendChild(td);
    };
    
    /**
     * Dessine un pion blanc sur le plateau
     * @param {Element} tr
     * @returns {undefined}
     */
    this.drawWhitePiece = function (tr) {
        var white_piece = '<svg width="40" height="40"><circle cx="20" cy="20" r="20" stroke="black" stroke-width="0" class="white_piece" /></svg>';
        var td = document.createElement("td");
        td.innerHTML = white_piece;
        tr.appendChild(td);
    };
    
    /**
     * Dessine une case vide mais marqué comme jouable sur le plateau
     * @param {Element} tr
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    this.drawMarkedSquare = function (tr, x, y) {
        var marked_piece = '<svg width="40" height="40"><circle cx="20" cy="20" r="5" stroke="black" stroke-width="0" class="marked_square" /></svg>';
        var td = document.createElement("td");
        td.setAttribute('onmouseover', 'plateau.mouseIn(' + x + ',' + y + ')');
        td.setAttribute('onmouseout', 'plateau.mouseOut()');
        td.setAttribute('onclick', 'plateau.mouseClick(' + x + ',' + y + ')');
        td.innerHTML = marked_piece;
        tr.appendChild(td);
    };

    /**
     * Dessine une case vide sur le plateau
     * @param {Element} tr
     * @returns {undefined}
     */
    this.drawVoidSquare = function (tr) {
        var td = document.createElement("td");
        tr.appendChild(td);
    };

    /**
     * Retourne l'état actuel du jeu
     * @returns {String}
     */
    this.toString = function() {
        var outputString = "";
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                outputString += this.board[i][j];
            }
        }
        return outputString;
    };

    /**
     * Sauvegarde le dernier coup joué
     * @returns {undefined}
     */
    this.saveState= function () {
        this.lastPlayer = this.player;
        for (var line = 0; line < 8; line++) {
            this.save[line] = [];
            for (var col = 0; col < 8; col++) {
                this.save[line][col] = this.board[line][col];
            }
        }
    };

    /**
     * Annule le dernier mouvement effectué
     * @returns {undefined}
     */
    this.cancelLastMove = function() {
        var isDifferent = false;
        for (var i = 0; i < this.board.length; i++) {
            for (var j = 0; j < this.board[i].length; j++) {
                if (this.board[i][j] !== this.save[i][j]) {
                    isDifferent = true;
                }
            }
        }

        if (isDifferent) {
            for (var line = 0; line < 8; line++) {
                for (var col = 0; col < 8; col++) {
                    this.board[line][col] = this.save[line][col];
                }
            }
            
            if (this.lastPlayer !== this.player) {
                this.switchPlayersJob();
            }
            
            this.drawBoard();
        }
    };

    /**
     * Rejoue la dernière action
     * @param {String} saveString
     * @returns {undefined}
     */
    this.loadState = function(saveString) {
        var seven = 0;
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                this.board[i][j] = saveString.charAt(i + j + seven);
            }
            seven += 7;
        }

        this.drawBoard();
    };
    
    this.saveGameFinished = function (game_save, game_score, game_victory) {
        $.ajax({url: 'scripts/ajax/save_game_finished.php',
               type: 'POST',
               data: {
                   game_save: game_save,
                   game_score: game_score,
                   game_victory: game_victory
               },
               success: function () {
                   
               },
               error: function (e) {
                   console.log(e.message);
               }
        });
    };
}
