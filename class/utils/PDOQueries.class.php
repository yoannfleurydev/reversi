<?php
    include_once "PDOConfig.class.php";
    include_once dirname(__FILE__) . "/../User.class.php";

    class PDOQueries {
        private $db;

        public function __construct() {
            $this->db = new PDOConfig();
        }


        public function selectUser($user_login) {
            $user = null;

            $response = $this->db->query("SELECT * FROM user WHERE user_login='$user_login'");
            while ($data = $response->fetch()) {
                $user = new User($data['user_id'], $data['user_login'], $data['user_pass'], $data['user_config']);
            }
            $response->closeCursor();
            return $user;
        }

        public function selectUserWhereUserId($user_id) {
            $user = null;

            $response = $this->db->query("SELECT * FROM user WHERE user_id='$user_id'");
            while ($data = $response->fetch()) {
                $user = new User($data['user_id'], $data['user_login'], $data['user_pass'], $data['user_config']);
            }
            $response->closeCursor();
            return $user;
        }

        public function selectUsers() {
            $users = null;

            $response = $this->db->query("SELECT * FROM user");
            while ($data = $response->fetch()) {
                $users[] = new User($data['user_id'], $data['user_login'], $data['user_pass'], $data['user_config']);
            }
            $response->closeCursor();

            return $users;
        }

        public function selectGame($game_id) {
            $game = null;

            $response = $this->db->query("SELECT * FROM game WHERE game_id='$game_id'");
            while ($data = $response->fetch()) {
                $game = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $game;
        }

        public function selectGamesWhereGameUserId($game_user_id) {
            $games = null;

            $response = $this->db->query("SELECT * FROM game WHERE game_user_id='$game_user_id'");
            while ($data = $response->fetch()) {
                $games[] = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $games;
        }

        public function selectGames() {
            $games = null;

            $response = $this->db->query("SELECT * FROM game");
            while ($data = $response->fetch()) {
                $games[] = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $games;
        }

        public function selectLastGame($game_user_id) {
            $game = null;

            $response = $this->db->query("SELECT * FROM game WHERE game_user_id = '$game_user_id' ORDER BY game_id DESC LIMIT 1");
            while ($data = $response->fetch()) {
                $game = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $game;
        }

        public function select10BestGames() {
            $games = null;

            $response = $this->db->query("SELECT * FROM game ORDER BY game_score DESC LIMIT 10");
            while ($data = $response->fetch()) {
                $games[] = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $games;
        }

        public function select10BestGamesWhereUserId($user_id) {
            $games = null;

            $response = $this->db->query("SELECT * FROM game WHERE game_user_id='$user_id' ORDER BY game_score DESC LIMIT 10");
            while ($data = $response->fetch()) {
                $games[] = new Game($data['game_id'], $data['game_user_id'], $data['game_victory'], $data['game_score'], $data['game_save']);
            }
            $response->closeCursor();
            return $games;
        }
        
        public function selectUserConfig($user_id) {
            $response = $this->db->query("SELECT user_config FROM user WHERE user_id = '$user_id';");
            while($data = $response->fetch()) {
                $user = $data;
            }
            $response->closeCursor();
            return $user['user_config'];
        }

        public function insertUser($user_login, $user_pass) {
            $this->db->exec("INSERT INTO user(user_login, user_pass) VALUES('$user_login', '$user_pass')");
        }

        public function insertGame($game_user_id, $game_save) {
            $this->db->exec("INSERT INTO game(game_user_id, game_save) VALUES('$game_user_id', '$game_save')");
        }
        
        public function insertGameFinished($game_user_id, $game_save, $game_score, $game_victory) {
            $this->db->exec("INSERT INTO game(game_user_id, game_save, game_score, game_victory) VALUES('$game_user_id', '$game_save', '$game_score', '$game_victory')");
        }

        public function updateGame($game_id, $game_save) {
            $this->db->exec("UPDATE game SET game_save='$game_save' WHERE game_id='$game_id'");
        }
        
        public function updateGameFinished($game_id, $game_save, $game_score, $game_victory) {
            $this->db->exec("UPDATE game SET game_save='$game_save', game_score='$game_score', game_victory='$game_victory' WHERE game_id='$game_id'");
        }
        
        public function updateUserConfig($user_id, $user_config) {
            $this->db->exec("UPDATE user SET user_config='$user_config' WHERE user_id='$user_id'");
        }
    }
