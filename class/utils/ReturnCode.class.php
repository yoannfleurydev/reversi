<?php

/**
 * La classe ErrorCode permet de gérer les codes erreurs.
 */
class ReturnCode {
    public static $VALID_SIGNUP        = 0b1;        // L'inscription est correcte.
    public static $INVALID_USER_LOGIN  = 0b10;       // Le login est malformé et/ou invalide.
    public static $INVALID_USER_PASS   = 0b100;      // Le mot de passe est invalide.
    public static $USER_LOGIN_EXISTS   = 0b1000;     // Le login existe déjà.
    public static $WEAK_USER_PASS      = 0b10000;    // Le mot de passe est trop faible.
    public static $WRONG_USER_PASS     = 0b100000;   // Le mot de passe ne correspond pas au login.

    public static function printMessage($CODE) {
        $message = "";

        if ($CODE >= ReturnCode::$WRONG_USER_PASS) {
            $message .= "<p class='error' id='status'>Vous n'avez pas saisi le bon mot de passe pour ce compte</p>\n";
            $CODE -= ReturnCode::$WRONG_USER_PASS;
        }
        if ($CODE >= ReturnCode::$WEAK_USER_PASS) {
            $message .= "<p class='error' id='status'>Votre mot de passe est trop faible</p>\n";
            $CODE -= ReturnCode::$WEAK_USER_PASS;
        }
        if ($CODE >= ReturnCode::$USER_LOGIN_EXISTS) {
            $message .= "<p class='error' id='status'>Cet identifiant n'est pas disponible</p>";
            $CODE -= ReturnCode::$USER_LOGIN_EXISTS;
        }
        if ($CODE >= ReturnCode::$INVALID_USER_PASS) {
            $message .= "<p class='error' id='status'>Le mot de passe est invalide</p>";
            $CODE -= ReturnCode::$INVALID_USER_PASS;
        }
        if ($CODE >= ReturnCode::$INVALID_USER_LOGIN) {
            $message .= "<p class='error' id='status'>L'identifiant est invalide</p>";
            $CODE -= ReturnCode::$INVALID_USER_LOGIN;
        }
        if ($CODE >= ReturnCode::$VALID_SIGNUP) {
            $message .= "<p class='valid' id='status'>Votre inscription est prise en compte</p>";
            $CODE -= ReturnCode::$VALID_SIGNUP;
        }

        return $message;
    }
}