<?php

class PDOConfig extends PDO {
    private $engine;
    private $host;
    private $database;
    private $user;
    private $pass;

    /**
     * Constructeur de la classe PDOConfig. Il permet de :
     * - lire le fichier (.ini) /config/db_config.ini
     * - initialiser les attributs de la classe PDOConfig
     * - initialiser le parent.
     *
     * Pour plus de détails, se référer à la classe PDO
     * ( http://php.net/manual/en/class.pdo.php )
     */
    public function __construct(){
        if (file_exists(__DIR__ . "/../../config/db_config.ini")) {
            $init_file_array = parse_ini_file(__DIR__ . "/../../config/db_config.ini");
        } else {
            echo "Le fichier de configuration de base de données est inexistant. (config/db_config.ini)";
            exit;
        }
        $this->engine   = $init_file_array['engine'];
        $this->host     = $init_file_array['host'];
        $this->database = $init_file_array['database'];
        $this->user     = $init_file_array['user'];
        $this->pass     = $init_file_array['pass'];

        $dns = $this->engine.':dbname='.$this->database.";host=".$this->host;
        parent::__construct( $dns, $this->user, $this->pass );
    }

    /**
     * @param mixed $engine
     */
    public function setEngine($engine) {
        $this->engine = $engine;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host) {
        $this->host = $host;
    }

    /**
     * @param mixed $database
     */
    public function setDatabase($database) {
        $this->database = $database;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass) {
        $this->pass = $pass;
    }
}