<?php


class Game {
    private $game_id;
    private $game_user_id;
    private $game_victory;
    private $game_score;
    private $game_save;

    /**
     * Constructeur de la classe Game
     * @param $game_id Identifiant numérique unique de la classe Game
     * @param $game_user_id Identifiant numérique unique de User
     * @param $game_victory Booléen permettant de dire si cette partie est victorieuse ou non.
     * @param $game_score Entier qui donne le nombre de pion du User sur le plateau.
     * @param $game_save Chaîne de caractères de sauvegarde de partie.
     */
    function __construct($game_id, $game_user_id, $game_victory, $game_score, $game_save)
    {
        $this->game_id = $game_id;
        $this->game_user_id = $game_user_id;
        $this->game_victory = $game_victory;
        $this->game_score = $game_score;
        $this->game_save = $game_save;
    }

    /**
     * @return mixed
     */
    public function getGameId()
    {
        return $this->game_id;
    }

    /**
     * @param mixed $game_id
     */
    public function setGameId($game_id)
    {
        $this->game_id = $game_id;
    }

    /**
     * @return mixed
     */
    public function getGameUserId()
    {
        return $this->game_user_id;
    }

    /**
     * @param mixed $game_user_id
     */
    public function setGameUserId($game_user_id)
    {
        $this->game_user_id = $game_user_id;
    }

    /**
     * @return mixed
     */
    public function getGameVictory()
    {
        return $this->game_victory;
    }

    /**
     * @param mixed $game_victory
     */
    public function setGameVictory($game_victory)
    {
        $this->game_victory = $game_victory;
    }

    /**
     * @return mixed
     */
    public function getGameScore()
    {
        return $this->game_score;
    }

    /**
     * @param mixed $game_score
     */
    public function setGameScore($game_score)
    {
        $this->game_score = $game_score;
    }

    /**
     * @return mixed
     */
    public function getGameSave()
    {
        return $this->game_save;
    }

    /**
     * @param mixed $game_save
     */
    public function setGameSave($game_save)
    {
        $this->game_save = $game_save;
    }


}