<?php

class User {
    private $user_id;
    private $user_login;
    private $user_pass;
    private $user_config;

    /**
     * Constructeur de la classe User.
     * @param $user_id     int    L'identifiant numérique unique de l'utilisateur
     * @param $user_login  string L'identifiant texte unique de l'utilisateur.
     * @param $user_pass   string Le mot de passe utilisateur.
     * @param $user_config string La chaîne de caractères définissant les paramètres de l'utilisateur.
     */
    function __construct($user_id, $user_login, $user_pass, $user_config) {
        $this->user_id = $user_id;
        $this->user_login = $user_login;
        $this->user_pass = $user_pass;
        $this->user_config = $user_config;
    }

    /**
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUserLogin()
    {
        return $this->user_login;
    }

    /**
     * @param mixed $user_login
     */
    public function setUserLogin($user_login)
    {
        $this->user_login = $user_login;
    }

    /**
     * @return mixed
     */
    public function getUserPass()
    {
        return $this->user_pass;
    }

    /**
     * @param mixed $user_pass
     */
    public function setUserPass($user_pass)
    {
        $this->user_pass = $user_pass;
    }

    /**
     * @return mixed
     */
    public function getUserConfig()
    {
        return $this->user_config;
    }

    /**
     * @param mixed $user_config
     */
    public function setUserConfig($user_config)
    {
        $this->user_config = $user_config;
    }
}