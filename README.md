# README du projet Reversi #

  Ce projet a été réalisé par [Patrice Pezas](mailto:patrice.pezas@etu.univ-rouen.fr) et [Yoann Fleury](yoann.fleury@etu.univ-rouen.fr).

![Capture.PNG](https://bitbucket.org/repo/zKBrko/images/1828328291-Capture.PNG)
  

## Sommaire

* Installation de l'application web
* Architecture de la base de données
* Qu'elles sont les fonctionnalité du site?
* Que ce passe-t-il au niveau de la sécurité?
* Langages, framework, outils et méthodes de codes utilisés pour ce projet

### Installation de l'applcation web

Il vous faut mettre les fichiers là où vous souhaitez que l'application soit.
Il vous faudra **absolument** créer le fichier db_config.ini dans un dossier config 
à la racine de l'application avec le contenu suivant:

    ; Fichier de configuration de la base de données.
    engine = mysql
    host = localhost
    database = reversi
    user = l-utilisateur
    pass = le_mot_de_passe
    
Vous trouverez également le script de création de la base de données dans le dossier `sql`.
Bonne installation et *bon jeu*.


### Architecture de la base de données

La base de données contient deux tables `user` et `game` dont voici un schéma simplifié: 

    user
        - user_id
        - user_login
        - user_pass
        - user_config
        
    game
        - game_id
        - game_user_id
        - game_victory
        - game_score
        - game_save

### Qu'elles sont les fonctionnalité du site?

#### Le jeu

Il est possible de :

* Faire un `CTRL+Z` pour annuler son dernier coup. (Ceci supprime bien évidemment le dernier coup de 
    l'intelligence artificielle.
* Faire un `CTRL+S` pour sauvegarder la partie en cours. Si l'utilisateur est 
    inscrit/connnecté cela aura pour but de créer une ligne pour ce jeu dans la base de données.
    Si dans le cas contraire l'utilisateur n'est pas inscrit/connecté, alors il pourra sauvegarder
    l'état actuel du jeu avec une chaîne de caractère.


### Que ce passe-t-il au niveau de la sécurité?

* Les mots de passe sont cryptés grâce à la fonction password_hash et l'agorithme BCRYPT.
* Les failles NTUI et XSS sont normalement toutes déjouées.


### Langages, framework, outils et méthodes de codes utilisés pour ce projet

#### Langages utilisés:

* **HTML5** pour sémantique du contenu ( **Front End** ).
* **CSS3** pour la mise en forme du contenu ( **Front End** ).
* **JavaScript** pour les intéractions entre l'utilisateur et l'application web ( **Front End** ).
* **PHP** pour interpréter les formulaires, faire le lien entre la base de données ( **Back End** ).

#### Outils utilisés:

* **JQuery** pour faciliter l'écriture d'évenement en JavaScript.
* **Git** pour faciliter la gestion de version de notre code 
    (utlisation des conventions AngularJS pour les commits).
* **Bitbucket.org** pour le dépôt distant, la gestion de bugs.